import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { CocktailService } from '../shared/cocktail.service';
import { Cocktail } from '../shared/cocktail.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit, OnDestroy {
  cocktails!: Cocktail[];
  loadingCocktails!: boolean;
  changeSubscription!: Subscription;
  fetchingSubscription!: Subscription;


  constructor(private cocktailService: CocktailService) {
  }

  ngOnInit(): void {
    this.changeSubscription = this.cocktailService.cocktailsChange.subscribe(cocktails => {
      this.cocktails = cocktails;
    });
    this.cocktailService.fetchCocktails();
    this.fetchingSubscription = this.cocktailService.fetchingCocktails.subscribe((isFetching: boolean) => {
      this.loadingCocktails = isFetching;
    })
  }

  ngOnDestroy() {
    this.changeSubscription.unsubscribe();
    this.fetchingSubscription.unsubscribe();
  }
}
