import { Component, Input, OnInit } from '@angular/core';
import { Cocktail } from '../../shared/cocktail.model';
import { Router } from '@angular/router';
import { CocktailService } from '../../shared/cocktail.service';

@Component({
  selector: 'app-cocktails',
  templateUrl: './cocktails.component.html',
  styleUrls: ['./cocktails.component.css']
})
export class CocktailsComponent implements OnInit {
  @Input() cocktail!: Cocktail;

  currentCocktail!: Cocktail;
  isCocktail!: boolean;

  modalOpen: boolean = false;

  constructor(private router: Router,
              private cocktailService: CocktailService) {
  }

  ngOnInit(): void {
  }

  openModal(id: string) {
    this.modalOpen = true;
    this.cocktailService.fetchCocktail(id).subscribe(cocktail => {
      if (cocktail) {
        this.currentCocktail = cocktail;
        console.log(this.currentCocktail.ingredients);
        this.isCocktail = true;
      } else {
        alert('error');
      }
    })
  }

  closeModal() {
    this.modalOpen = false;
  }

}
