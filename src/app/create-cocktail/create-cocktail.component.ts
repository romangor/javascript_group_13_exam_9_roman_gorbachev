import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { Cocktail } from '../shared/cocktail.model';
import { CocktailService } from '../shared/cocktail.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-create-cocktail',
  templateUrl: './create-cocktail.component.html',
  styleUrls: ['./create-cocktail.component.css']
})
export class CreateCocktailComponent implements OnInit, OnDestroy {
  form!: FormGroup;
  load!: boolean;
  loadSubscription!: Subscription;

  constructor(private cocktailService: CocktailService) {
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      nameCocktail: new FormControl('', Validators.required),
      imageURL: new FormControl('', Validators.required),
      type: new FormControl('', Validators.required),
      description: new FormControl(''),
      ingredients: new FormArray([]),
      instruction: new FormControl('', Validators.required)
    });
    this.loadSubscription = this.cocktailService.sendingCocktail.subscribe((isLoad: boolean) => {
      this.load = isLoad;
    });
  }

  getIngredientsControls() {
    const steps = <FormArray>(this.form.get('ingredients'));
    return steps.controls;
  }

  addIngredient() {
    const ingredients = <FormArray>this.form.get('ingredients');
    const ingredientsGroup = new FormGroup({
      ingredientName: new FormControl('', Validators.required),
      amount: new FormControl('', Validators.required),
      unit: new FormControl('', Validators.required),
    });
    ingredients.push(ingredientsGroup);
  }

  removeIngredient(i: number) {
    const ingredients = <FormArray>this.form.get('ingredients');
    ingredients.removeAt(i);
  }

  createCocktail() {
    const cocktail = new Cocktail(
      this.form.value.nameCocktail,
      this.form.value.imageURL,
      this.form.value.type,
      this.form.value.description,
      this.form.value.ingredients,
      this.form.value.instruction,
      ''
    )
    this.cocktailService.sendCocktail(cocktail);
  }

  fieldHasError(fieldName: string, errorType: string) {
    const field = this.form.get(fieldName);
    return Boolean(field && field.touched && field.errors?.[errorType]);
  }

  ngOnDestroy() {
    this.loadSubscription.unsubscribe();
  }
}
