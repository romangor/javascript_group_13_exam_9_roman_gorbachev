import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';
import { Directive } from '@angular/core';


export const validatorURL = (control: AbstractControl): ValidationErrors | null => {
  const url = control.value.substring(0, 7);
  const urlS = control.value.substring(0, 8);
  if (url === 'http://' || urlS === 'https://') {
    return null;
  }
  return {urlInCorrect: true};
}

@Directive({
  selector: '[appUrl]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: ValidateURLDirective,
    multi: true
  }]
})

export class ValidateURLDirective implements Validator {
  validate(control: AbstractControl): ValidationErrors | null {
    return validatorURL(control);
  }
}
