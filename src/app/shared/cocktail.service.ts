import { Injectable } from '@angular/core';
import { Cocktail } from './cocktail.model';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';

@Injectable()
export class CocktailService {
  cocktails!: Cocktail[];

  sendingCocktail = new Subject<boolean>();
  cocktailsChange = new Subject<Cocktail[]>();
  fetchingCocktails = new Subject<boolean>();

  constructor(private http: HttpClient,
              private router: Router) {
  }

  sendCocktail(cocktail: Cocktail) {
    this.sendingCocktail.next(true);
    const body = {
      nameCocktail: cocktail.nameCocktail,
      imageURL: cocktail.imageURL,
      type: cocktail.type,
      description: cocktail.description,
      ingredients: cocktail.ingredients,
      instruction: cocktail.instruction
    }
    this.http.post('https://projectsattractor-default-rtdb.firebaseio.com/cocktails.json', body).subscribe(() => {
      this.sendingCocktail.next(false);
      void this.router.navigate(['/']);
    });
  }

  fetchCocktails() {
    this.fetchingCocktails.next(true);
    this.http.get<{ [id: string]: Cocktail }>('https://projectsattractor-default-rtdb.firebaseio.com/cocktails.json')
      .pipe(map(result => {
        if (result === null) {
          return [];
        }
        return Object.keys(result).map(id => {
          const cocktailData = result[id];
          return new Cocktail(
            cocktailData.nameCocktail,
            cocktailData.imageURL,
            cocktailData.type,
            cocktailData.description,
            cocktailData.ingredients,
            cocktailData.instruction,
            id);
        });
      }))
      .subscribe(cocktails => {
        this.cocktails = cocktails;
        this.cocktailsChange.next(this.cocktails.slice());
        this.fetchingCocktails.next(false);
      })
  }

  fetchCocktail(id: string) {
    return this.http.get<Cocktail | null>(`https://projectsattractor-default-rtdb.firebaseio.com/cocktails/${id}.json`)
      .pipe(map(result => {
        if (!result) return null;
        return new Cocktail(result.nameCocktail, result.imageURL, result.type, result.description, result.ingredients, result.instruction, id);
      }));
  }


}
