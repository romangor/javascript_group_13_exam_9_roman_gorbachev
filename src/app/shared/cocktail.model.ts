export class Cocktail {
  constructor(public nameCocktail: string,
              public imageURL: string,
              public type: string,
              public description: string,
              public ingredients: [{
                ingredientName: string,
                amount: number,
                unit: string,
              }],
              public instruction: string,
              public id: string) {
  }
}
