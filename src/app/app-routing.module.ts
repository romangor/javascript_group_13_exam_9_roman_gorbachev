import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateCocktailComponent } from './create-cocktail/create-cocktail.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'create/cocktail', component: CreateCocktailComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
