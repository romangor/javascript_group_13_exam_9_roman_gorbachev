import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { CocktailsComponent } from './home/cocktails/cocktails.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { CreateCocktailComponent } from './create-cocktail/create-cocktail.component';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { CocktailService } from './shared/cocktail.service';
import { ModalComponent } from './modal/modal.component';
import { ValidateURLDirective } from './directives/validate-url.directive';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CocktailsComponent,
    ToolbarComponent,
    CreateCocktailComponent,
    ModalComponent,
    ValidateURLDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [CocktailService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
